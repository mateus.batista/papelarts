from django.urls import path
from rest_framework.urlpatterns import format_suffix_patterns

from .views import ProdutoList, ProdutoDetail, VendedorList, ClienteDetail, ClienteList, VendedorDetail, VendaList, \
    VendaDetail, ComissaoSerializer

urlpatterns = [
    path('api/produtos/', ProdutoList.as_view()),
    path('api/produtos/<int:pk>/', ProdutoDetail.as_view()),
    path('api/clientes/', ClienteList.as_view()),
    path('api/clientes/<int:pk>/', ClienteDetail.as_view()),
    path('api/vendedores/', VendedorList.as_view()),
    path('api/vendedores/<int:pk>/', VendedorDetail.as_view()),
    path('api/vendas/', VendaList.as_view()),
    path('api/vendas/<int:pk>/', VendaDetail.as_view()),
    path('api/comissoes/', ComissaoSerializer.as_view()),
]
urlpatterns = format_suffix_patterns(urlpatterns)
