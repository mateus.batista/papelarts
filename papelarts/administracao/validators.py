from django.core.exceptions import ValidationError


def telefone_valido(value):
    if len(value) < 11:
        raise ValidationError('O telefone deve estar no formato DDDXXXXXXXXX')
    try:
        int(value)
    except Exception:
        raise ValidationError('Este campo não aceita letras ou caracteres')