from datetime import date

from django.db.models import Sum
from django.http import JsonResponse
from rest_framework import generics, status

from .models import Produto, Vendedor, Cliente, Venda, ItemVenda
from .serializers import ProdutoSerializer, VendedorSerializer, ClienteSerializer, VendaSerializer


# Create your views here.
class ProdutoList(generics.ListCreateAPIView):
    queryset = Produto.objects.all()
    serializer_class = ProdutoSerializer


class ProdutoDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Produto.objects.all()
    serializer_class = ProdutoSerializer


class ClienteList(generics.ListCreateAPIView):
    queryset = Cliente.objects.all()
    serializer_class = ClienteSerializer


class ClienteDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Cliente.objects.all()
    serializer_class = ClienteSerializer


class VendedorList(generics.ListCreateAPIView):
    queryset = Vendedor.objects.all()
    serializer_class = VendedorSerializer


class VendedorDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Vendedor.objects.all()
    serializer_class = VendedorSerializer


class VendaList(generics.ListCreateAPIView):
    queryset = Venda.objects.all()
    serializer_class = VendaSerializer


class ComissaoVendedoresList(generics.ListCreateAPIView):
    queryset = Venda.objects.all()
    serializer_class = VendaSerializer


class VendaDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Venda.objects.all()
    serializer_class = VendaSerializer


class ComissaoSerializer(generics.ListAPIView):
    """
    codigo_vendedor
    nome_vendedor
    total_vendas
    total_comissoes_vendedor
    """

    def get(self, request):
        data_inicio = self.request.GET.get('data_inicio')
        data_fim = self.request.GET.get('data_fim')
        vendedores = Vendedor.objects.all()
        data = []
        for index, vendedor in enumerate(vendedores):
            vendas = Venda.objects.filter(data_hora__range=(data_inicio, data_fim), vendedor=vendedor)
            if len(vendas) != 0:
                data.append(
                    {
                        "codigo_vendedor": vendedor.id,
                        "nome_vendedor": vendedor.nome,
                        "total_vendas": 0,
                        "total_comissoes": 0.0,
                    }
                )
                for venda in vendas:
                    data[index]["total_vendas"] += 1
                    data[index]["total_comissoes"] += venda.lista_produtos.aggregate(Sum('valor_comissao'))[
                                                          'valor_comissao__sum'] or 0
            else:
                data = {
                    "error_mensage": "Não existe vendas realizadas no período informado"
                }
                return JsonResponse(data, safe=False, status=status.HTTP_404_NOT_FOUND)

        return JsonResponse(data, safe=False, status=status.HTTP_200_OK)
