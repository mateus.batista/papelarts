import datetime
from rest_framework import serializers
from .models import Produto, Cliente, Vendedor, Venda, ItemVenda, LimiteComissao

class ProdutoSerializer(serializers.ModelSerializer):
    class Meta:
        model = Produto
        fields = '__all__'


class ClienteSerializer(serializers.ModelSerializer):
    class Meta:
        model = Cliente
        fields = '__all__'


class VendedorSerializer(serializers.ModelSerializer):
    class Meta:
        model = Vendedor
        fields = '__all__'


class ItemVendaSerializer(serializers.ModelSerializer):
    class Meta:
        model = ItemVenda
        fields = '__all__'


class VendaSerializer(serializers.ModelSerializer):
    lista_produtos = ItemVendaSerializer(many=True)

    class Meta:
        model = Venda
        fields = '__all__'

    def create_or_update_itens_venda(self, itens_venda):
        item_venda_ids = []
        for item_venda in itens_venda:
            item_venda['produto'] = Produto.objects.get(pk=item_venda['produto'])

            valor_produto = item_venda['produto'].valor_unitario
            comissao_produto = item_venda['produto'].percentual_comissao
            dia_semana = datetime.datetime.today().weekday()
            try:
                limites_comissao = LimiteComissao.objects.get(dia_semana=dia_semana)
                if comissao_produto <= limites_comissao.minimo_comissao:
                    comissao_produto = limites_comissao.minimo_comissao
                elif comissao_produto >= limites_comissao.maximo_comissao:
                    comissao_produto = limites_comissao.maximo_comissao
            except:
                pass

            item_venda['valor_comissao'] = round(valor_produto * comissao_produto * item_venda['quantidade'], 2)
            item_venda_instance, created = ItemVenda.objects.update_or_create(pk=item_venda.get('id'),
                                                                              defaults=item_venda)
            item_venda_ids.append(item_venda_instance.pk)
        return item_venda_ids

    def create(self, validated_data):
        item_venda = self.initial_data.pop('lista_produtos', [])
        validated_data.pop('lista_produtos', [])
        venda = Venda.objects.create(**validated_data)
        venda.lista_produtos.set(self.create_or_update_itens_venda(item_venda))
        return venda

    def update(self, instance, validated_data):
        item_venda = self.initial_data.pop('lista_produtos', [])
        instance.lista_produtos.set(self.create_or_update_itens_venda(item_venda))
        fields = ['numero_nota_fiscal', 'vendedor', 'cliente']
        for field in fields:
            try:
                setattr(instance, field, validated_data[field])
            except KeyError:
                pass
        instance.save()
        return instance

class ComissaoSerializer(serializers.ModelSerializer):
    codigo_vendedor = serializers.IntegerField()
    nome_vendedor = serializers.CharField(max_length=255)
    total_vendas = serializers.FloatField()
    total_comissoes_vendedor = serializers.FloatField()
