from django.core.validators import MinValueValidator, MaxValueValidator
from django.db import models
from .validators import telefone_valido


# Create your models here.

class Produto(models.Model):
    codigo = models.PositiveIntegerField(verbose_name='Código', unique=True)
    descricao = models.CharField(verbose_name='Descrição', max_length=100)
    valor_unitario = models.FloatField(verbose_name='Valor Unitário', )
    percentual_comissao = models.FloatField(verbose_name='Percentual da Comissão',
                                            help_text='O valor deve ser entre 0 e 10',
                                            validators=[MinValueValidator(0), MaxValueValidator(10)])

    class Meta:
        verbose_name = 'Produto'
        verbose_name_plural = 'Produtos'

    def __str__(self):
        return f'{self.codigo} - {self.descricao}'


# TODO: APLICAR MASCARA DE TELEFONE NOS MODELOS DE VENDEDOR E CLIENTE
class Vendedor(models.Model):
    nome = models.CharField(verbose_name='Nome', max_length=255)
    telefone = models.CharField(verbose_name='Telefone',max_length=11, validators=[telefone_valido])
    email = models.EmailField(verbose_name='E-mail')

    class Meta:
        verbose_name = 'Vendedor'
        verbose_name_plural = 'Vendedores'

    def __str__(self):
        return self.nome


class Cliente(models.Model):
    nome = models.CharField(verbose_name='Nome', max_length=255)
    telefone = models.CharField(verbose_name='Telefone', max_length=11, validators=[telefone_valido])
    email = models.EmailField(verbose_name='E-mail')

    class Meta:
        verbose_name = 'Cliente'
        verbose_name_plural = 'Clientes'

    def __str__(self):
        return self.nome


class ItemVenda(models.Model):
    produto = models.ForeignKey(Produto, on_delete=models.CASCADE, verbose_name='Produto')
    quantidade = models.PositiveIntegerField(verbose_name='Quantidade')
    valor_comissao = models.FloatField(verbose_name='Valor da Comissão em R$', blank=True, null=True)

    def __str__(self):
        return f'{self.produto.descricao} - Qtd: {self.quantidade} -' \
               f' Valor: {self.quantidade * self.produto.valor_unitario}'


class Venda(models.Model):
    numero_nota_fiscal = models.PositiveIntegerField(verbose_name='Número da Nota Fiscal')
    data_hora = models.DateTimeField(verbose_name='Data e Hora', auto_now=True)
    vendedor = models.ForeignKey(Vendedor, on_delete=models.CASCADE, verbose_name='Vendendor')
    cliente = models.ForeignKey(Cliente, on_delete=models.CASCADE, verbose_name='Cliente')
    lista_produtos = models.ManyToManyField(ItemVenda, verbose_name='Lista de Produtos', blank=True)

    def __str__(self):
        return f'Nota Fiscal: {self.numero_nota_fiscal} - Vendedor: {self.vendedor}'


class LimiteComissao(models.Model):
    DIA_SEMANA_CHOICES = [
        (0, 'Segunda-Feira'),
        (1, 'Terça-Feira'),
        (2, 'Quarta-Feira'),
        (3, 'Quinta-Feira'),
        (4, 'Sexta-Feira'),
        (5, 'Sábado'),
        (6, 'Domingo'),
    ]

    minimo_comissao = models.PositiveIntegerField(verbose_name='Comissão Mínima')
    maximo_comissao = models.PositiveIntegerField(verbose_name='Comissão Máxima')
    dia_semana = models.PositiveIntegerField(choices=DIA_SEMANA_CHOICES, default=0, verbose_name='Dia da Semana',
                                             unique=True)

    def __str__(self):
        return f'{self.get_dia_semana_display()} - Comissão Mínima:{self.minimo_comissao}% - ' \
               f'Comissão Máxima:{self.maximo_comissao}%'