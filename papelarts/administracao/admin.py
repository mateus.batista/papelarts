from django.contrib import admin
from .models import Produto, Vendedor, Cliente, LimiteComissao, Venda, ItemVenda

# Register your models here.

admin.site.register(Produto)
admin.site.register(Vendedor)
admin.site.register(Cliente)
admin.site.register(LimiteComissao)
admin.site.register(Venda)
admin.site.register(ItemVenda)
